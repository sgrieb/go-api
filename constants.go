package main

import "os"

type _Constants struct {
	GET_ACCOUNTS string
	DB           string
}

func Constants() _Constants {
	constants := _Constants{}
	constants.DB = os.Getenv("DB")

	constants.GET_ACCOUNTS = "SELECT * FROM account"

	return constants
}

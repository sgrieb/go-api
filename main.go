package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
)

func main() {

	// fmt.Println("Test")

	// err := godotenv.Load()
	// if err != nil {
	// 	log.Fatal("Error loading .env file")
	// }

	router := NewRouter()

	db, err := sql.Open("sqlite3", "./data/db.db")
	checkErr(err)

	rows, err := db.Query(Constants().GET_ACCOUNTS)
	checkErr(err)

	var id int
	var name string

	for rows.Next() {
		err = rows.Scan(&id, &name)
		checkErr(err)
		fmt.Println(id)
		fmt.Println(name)
	}

	log.Fatal(http.ListenAndServe(":3000", router))
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
